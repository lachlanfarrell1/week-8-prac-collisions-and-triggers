﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {

    public AudioClip scoreClip;
    private AudioSource audio;

    void Start ()
    {
        audio = GetComponent<AudioSource>();
    }

    void OnTriggerEnter (Collider collider)
    {
        audio.PlayOneShot(scoreClip);
        PuckControl puck = collider.gameObject.GetComponent<PuckControl>();
        puck.ResetPosition();

        PuckControl AIPlayer = collider.gameObject.GetComponent<PuckControl>();
        AIPlayer.ResetPosition();
            }
}

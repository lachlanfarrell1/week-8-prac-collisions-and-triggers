﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class KeyMove : MonoBehaviour {

    Rigidbody rb;

	void Start () {
        rb.GetComponent<Rigidbody>();
        rb.useGravity = false;
	}
	

    public float Speed = 20f;

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, moveVertical, 0.0f);

        rb.AddForce(movement * Speed);
    }
}
